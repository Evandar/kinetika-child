<?php
/*
Template Name: Full Blank Page
*/
?>

<?php if (have_posts()) : ?>
    <?php while (have_posts()) : the_post(); ?>

            <div>
            <?php
            $isactive = get_post_meta( get_the_id(), "mtheme_pb_isactive", true );
            if (isSet($isactive) && $isactive==1) {
                $builder_data = do_shortcode('[template id="'.$post->ID.'"]');
                echo $builder_data;
            } else {
                the_content();
                wp_link_pages( array( 'before' => '<div class="page-link">' . __( 'Pages:', 'mthemelocal' ), 'after' => '</div>' ) );
            }
            ?>
            </div>

    <?php endwhile; else: ?>
<?php endif; ?>
