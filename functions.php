<?php
add_action( 'wp_enqueue_scripts', 'my_theme_enqueue_styles' );
function my_theme_enqueue_styles() {
    wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
//    wp_enqueue_style( 'child-style', get_stylesheet_directory_uri() . '/css/schedule.css' );
    wp_enqueue_style( 'child-style', get_stylesheet_directory_uri() . '/css/style.css' );
    wp_enqueue_style( 'bootstrap', get_stylesheet_directory_uri() . '/css/bootstrap.min.css' );

}

function load_js() {
//    wp_enqueue_script('script', get_stylesheet_directory_uri() . '/js/schedule.js', array( 'jquery' ));
    wp_enqueue_script('script', get_stylesheet_directory_uri() . '/js/main.js', array( 'jquery' ));
    wp_enqueue_script('script', get_stylesheet_directory_uri() . '/js/modernizr.js', array( 'jquery' ));
    wp_enqueue_script('bootstrap script', get_stylesheet_directory_uri() . '/js/bootstrap.min.js', array( 'jquery' ));

}
add_action( 'wp_enqueue_scripts' , 'load_js' );

show_admin_bar( false );
add_theme_support( 'post-thumbnails', array( 'post', 'page' ) );



function my_mtheme_widgets_init() {
// Footer 3 columns
	register_sidebar(array(
		'name' => 'Footer Three Columns 1',
		'id' => 'footer_13',
		'before_widget' => '<div class="sidebar-widget"><aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside></div>',
		'before_title' => '<h3>',
		'after_title' => '</h3>',
	));

// Footer 3 columns
	register_sidebar(array(
		'name' => 'Footer Three Columns 2',
		'id' => 'footer_23',
		'before_widget' => '<div class="sidebar-widget"><aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside></div>',
		'before_title' => '<h3>',
		'after_title' => '</h3>',
	));

// Footer 3 columns
	register_sidebar(array(
		'name' => 'Footer Three Columns 3',
		'id' => 'footer_33',
		'before_widget' => '<div class="sidebar-widget"><aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside></div>',
		'before_title' => '<h3>',
		'after_title' => '</h3>',
	));

    // Slot for registration form
    register_sidebar(array(
        'name' => 'RegistrationFrom',
        'id' => 'registration_form',
        'before_widget' => '<div class="sidebar-widget"><aside id="%1$s" class="widget %2$s">',
        'after_widget' => '</aside></div>',
        'before_title' => '<h3>',
        'after_title' => '</h3>',
    ));
}
add_action( 'widgets_init', 'my_mtheme_widgets_init' );
