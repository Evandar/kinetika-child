<?php get_header(); ?>

<div id="homepage">
    <div class="entry-page-wrapper entry-content clearfix">
        <div class="mtheme-block mtheme-block-em_heroimage span12 mtheme-first-cell " data-width="12">
            <div class="bgimage-wrap -title textlocation-top intensity-default mtheme-parallax" data-stellar-background-ratio="2" data-stellar-horizontal-offset="40"
                 data-stellar-vertical-offset="350" style="background-image: url(<?php echo get_stylesheet_directory_uri();?>/img/pot_front.jpg); background-position: 0 100%">
                <div class="hero-text-wrap">
                    <ul class="none single">
                        <li>
                            <h2 class="hero-title">POTAŃCÓWKI</h2>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="container clearfix">
            <div class="page-contents-wrap  ">
                <div id="post-16" class="post-16 page type-page status-publish has-post-thumbnail hentry">
                    <div class="entry-page-wrapper entry-content clearfix">
                        <div class="mtheme-modular-column  column-parallax"
                             style="padding-bottom:56px;padding-top:112px; ">
                            <div class="mtheme-supercell clearfix  boxed-column">
                                <div class="column-setter first-column span12">
                                    <div class="mtheme-cell-wrap">
                                        <div class="mtheme-cell-inner">
                                            <div class="row clearfix">
                                                <div class="mtheme-cell-wrap">
                                                    <div id="mtheme-block-4"
                                                         class="mtheme-block mtheme-block-em_dividers span1 mtheme-first-cell "
                                                         data-width="1">
                                                        <div class="mtheme-cell-inner">
                                                            <div class="clearfix divider-common default-divider  divider-blank"
                                                                 style="padding-top:10px;"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="mtheme-cell-wrap">
                                                    <div id="mtheme-block-5"
                                                         class="mtheme-block mtheme-block-em_multiheadline span10 mtheme-following-cell "
                                                         data-width="10">
                                                        <div class="mtheme-cell-inner">
                                                            <div class="multi-headlines-outer"
                                                                 style="margin-bottom:38px;">
                                                                <div class="multi-headlines-wrap">
                                                                    <div class="entry-content postformat_contents clearfix">
                                                                        <span class="quote_say"><i class="fa fa-quote-left"></i> Taniec jest jak napad na bank - liczy się każda sekunda.<i class="fa fa-quote-right"></i></span>
                                                                        <span class="quote_author">—&nbsp;Twyla Tharp</span>
                                                                        <div class="fullcontent-spacing"><article></article></div></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row clearfix">
                                                <div class="mtheme-cell-wrap">
                                                    <div id="mtheme-block-4"
                                                         class="mtheme-block mtheme-block-em_dividers span2 mtheme-first-cell "
                                                         data-width="1">
                                                        <div class="mtheme-cell-inner">
                                                            <div class="clearfix divider-common default-divider  divider-blank"
                                                                 style="padding-top:10px;"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="mtheme-cell-wrap">
                                                    <div id="mtheme-block-5"
                                                         class="mtheme-block mtheme-block-em_multiheadline span8 mtheme-following-cell "
                                                         data-width="10">
                                                        <div class="mtheme-cell-inner">
                                                            <div class="multi-headlines-outer"
                                                                 style="margin-bottom:38px;">
                                                                <div class="multi-headlines-wrap">
                                                                    <div class="multi-headlines animated fadeInUp animation-action">
                                                                        <h2 style="text-align: center;">Tancerze, miłośnicy jazzu, stylu vintage i świetnej zabawy!</h2>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row clearfix">
                                                <div class="mtheme-cell-wrap">
                                                    <div class="mtheme-block mtheme-block-em_dividers span2 mtheme-first-cell" data-width="2">
                                                        <div class="mtheme-cell-inner">
                                                            <div class="clearfix divider-common default-divider  divider-blank"
                                                                 style="padding-top:10px;"></div>
                                                        </div>
                                                    </div>
                                                    <div class="mtheme-block mtheme-block-em_displayrichtext span8 mtheme-following-cell" data-width="8">
                                                        <div class="mtheme-cell-inner">
                                                            <div class="animated fadeInUpSlight animation-action" style="text-align: center;">
                                                                <p>Zapraszamy na <strong>Roar! Swing</strong>, czyli potańcówki w Paszczy Lwa. </p>
                                                                <p>Tańczymy w każdy poniedziałek od 19:00-23:00. Zapraszamy wszystkich, którzy chcieliby zasmakować swinga! Strój jest dowolny, chociaż elementy retro/vintage są zawsze mile widziane!</p>
                                                                <p>Wstęp: na wejściu/wyjściu wrzuć kilka monet do czekającego na barze kapelusza – jest to zrzutka dla obsługi lokalu.</p>
                                                                <p>Zerknij na swingowe potańcówki okiem Tomka i reszty ferajny oraz posłuchaj rozmów niekontrolowanych przy piwie. :)</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row clearfix">
                                                <div class="mtheme-cell-wrap">
                                                    <div id="mtheme-block-4"
                                                         class="mtheme-block mtheme-block-em_dividers span2 mtheme-first-cell "
                                                         data-width="2">
                                                        <div class="mtheme-cell-inner">
                                                            <div class="clearfix divider-common default-divider  divider-blank"
                                                                 style="padding-top:10px;"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="mtheme-cell-wrap">
                                                    <div id="mtheme-block-6"
                                                         class="mtheme-block mtheme-block-em_dividers span8 mtheme-following-cell "
                                                         data-width="8">
                                                        <div class="mtheme-cell-inner">
                                                            <div class="portfolio-details-inner events-details-wrap">
                                                                <div class="entry-content">
                                                                    <div class="event-details-column event-details-column-one column2 column_space">
                                                                        <h3 class="event-heading"><i style="font-size:24px; color: #fe4641 !important;"
                                                                                                     class="shortcode-fontawesome-icon fa fa-map-marker"></i>
                                                                        </h3>
                                                                        <h3>Paszcza Lwa</h3>
                                                                        <p> Kwietna 39, Gdańsk</p>
                                                                        <h3>
                                                                            <i style="font-size:24px; color: #fe4641 !important;"
                                                                               class="shortcode-fontawesome-icon fa fa-clock-o"></i></i>
                                                                            19.00 - 23.00</h3>
                                                                        <h3>
                                                                            <i style="font-size:24px; color: #fe4641 !important;"
                                                                               class="shortcode-fontawesome-icon fa fa-calendar"></i>
                                                                            Każdy poniedziałek</h3>
                                                                        <h3>
                                                                            <i style="font-size:24px; color: #fe4641 !important;"
                                                                               class="shortcode-fontawesome-icon fa fa-money"></i>
                                                                            Kilka złotych monet</h3>
                                                                    </div>
                                                                    <div class="event-details-column column2">


                                                                                <iframe width="400" height="300" id="gmap_canvas" src="https://maps.google.com/maps?q=Paszcza%20Lwa&t=&z=17&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
                                                                    </div>
                                                                </div>
                                                                <div class="clearfix"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div> <!-- mtheme-cell-inner -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div><!-- .entry-content -->
                </div><!--post-16 -->
            </div><!-- page-contents-wrap -->
        </div><!-- container -->
        <div class="mtheme-block mtheme-block-em_heroimage span12 mtheme-first-cell " data-width="12">
            <div class="bgimage-wrap textlocation-top intensity-default mtheme-parallax" data-stellar-background-ratio="0.5" style="background-image: url(<?php echo get_stylesheet_directory_uri();?>/img/shoes.jpg); height:auto !important;">
                <div class="mtheme-modular-column  column-parallax"
                     style="padding-bottom:50px;padding-top:50px; ">
                    <div class="mtheme-supercell clearfix  boxed-column">
                        <div class="column-setter first-column span12">
                            <div class="mtheme-cell-wrap">
                                <div class="mtheme-cell-inner">
                                    <div class="row clearfix">
                                        <div class="mtheme-cell-wrap">
                                            <div class="mtheme-block mtheme-block-em_dividers span2 mtheme-first-cell" data-width="2">
                                                <div class="mtheme-cell-inner">
                                                    <div class="clearfix divider-common default-divider  divider-blank"
                                                         style="padding-top:10px;"></div>
                                                </div>
                                            </div>
                                            <div class="mtheme-block mtheme-block-em_displayrichtext span8 mtheme-following-cell" data-width="8">
                                                <div class="mtheme-cell-inner">
                                                    <div class="animated fadeInUpSlight animation-action" style="text-align: center;">
                                                        <h2>Zerknij na swingowe potańcówki okiem Tomka i reszty ferajny oraz posłuchaj rozmów niekontrolowanych przy piwie. :)</h2>
                                                        <iframe style="padding: 20px;" width="420" height="315"
                                                                src="http://www.youtube.com/embed/Inb4f0QNejg?autoplay=0&amp;rel=0&amp;enablejsapi=1&amp;wmode=opaque">
                                                        </iframe>
                                                        <h2>W skrócie: jeśli masz apetyt na wyjątkowy taniec w doborowym towarzystwie doprawiony porządną dawką klimatów retro – dobrze trafiłeś!
                                                        </h2>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div> <!-- mtheme-cell-inner -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- .entry-content -->
</div>

<?php get_footer(); ?>