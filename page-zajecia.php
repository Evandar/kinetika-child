<?php get_header(); ?>

    <div id="homepage">
        <div class="entry-page-wrapper entry-content clearfix">
            <div class="bgimage-wrap mtheme-parallax mtheme-modular-column" data-stellar-background-ratio="0.5" style="background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/img/pot_front.jpg); height:auto !important;">

                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="text-align-center">
                                <div class="hero-title-wrapper">
                                    <div class="hero-title">Zajęcia</div>
                                </div>
<!--                                <h1 class="hero-title">Zajęcia</h1>-->
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <div class="container clearfix">
                <section id="grafik">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="section-heading text-align-center">
                                <h1 class="section-title">Grafik</h1>
                                <div class="section-end"></div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="cd-schedule animation-completed">
                                <div class="timeline">
                                </div> <!-- .timeline -->

                                <div class="events">
                                </div>

                                <div class="event-modal">
                                    <header class="header">
                                        <div class="content">
                                            <div class="event-time"></div>
                                            <h3 class="event-name"></h3>
                                            <div class="details">
                                                <a href="#" class="event-place"><i class="fa fa-map-marker"></i> <p></p></a>
                                                <div class="event-instructors"><i class="fa fa-user-secret" ></i> <p></p></div>
                                                <a href="#" class="event-group"><i class="fa fa-users" ></i> <p></p></a>
                                                <div class="event-date"><i class="fa fa-calendar" ></i> <p></p></div>
                                                <div class="event-price"><i class="fa fa-money" ></i> <p></p></div>
                                                <a class="event-registration"><i class="fa fa-user-plus" ></i> <p></p></a>

                                            </div>
                                        </div>

                                        <div class="header-bg"></div>
                                    </header>

                                    <div class="details-body">
                                        <div class="content">
                                        </div>
                                    </div>

                                    <a href="#0" class="close"></a>
                                </div>

                                <div class="cover-layer"></div>
                            </div> <!-- .cd-schedule -->
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12 text-align-center">
                            <p>
                                Zapisująć się na nasze zajęcia, zgadzasz się na nasz regulamin zajęć:
                            </p>
                            <a class="mtheme-button animated pulse animation-action regulamin-btn">Regulamin zajęć</a>
                        </div>
                    </div>
                </section> <!-- Rejestracja -->

                <section id="cennik">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="section-heading text-align-center">
                                <h1 class="section-title">Cennik</h1>
                                <div class="section-end"></div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-10 offset-lg-1">
                            <table class="price-table">
                                <tr>
                                    <td class="price-cell">
                                        <div class="title">
                                            Karnet miesięczny
                                        </div>
                                        <div class="price">
                                            120 zł
                                        </div>
                                    </td>
                                    <td class="price-description">
                                        <p>Karnet miesięczny na jeden kurs (4 zajęcia).</p>
                                        <p>W zależności od ilości zajęć w danym miesiącu cena będzie proporcjonalnie zmniejszana lub zwiększana.
                                            Przy 3 zajęciach: <strong>90zł</strong>, przy 5 zajęciach: <strong>150zł</strong> <br></p>

                                        <p class="small-info">Informację o ilości zajęć w danym miesiącu będziemy przekazywać z wyprzedzeniem (przed pierwszymi zajęciami w miesiącu).</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="price-cell">
                                        <div class="title">
                                            Wejście pojedyncze
                                        </div>
                                        <div class="price">
                                            35 zł
                                        </div>
                                    </td>
                                    <td class="price-description">
                                        <p>Cena za pojedyncze zajęcia. Jeżeli nie zapłacisz do pierwszych zajęć z góry obowiązuje Cie cena za pojedyncze zajęcia.</p>
                                        <p class="small-info">Wejście pojedyncze jest dostępne dla grup LH1+</p>

                                    </td>
                                </tr>
                                <tr>
                                    <td class="price-cell">
                                        <div class="title">
                                            Bloki tematyczne
                                        </div>
                                        <div class="price">
                                            <i class="fa fa-question"></i>
                                        </div>
                                    </td>
                                    <td class="price-description">
                                        Cena bloku tematycznego będzie podawana każdorazowo przy ogłaszaniu nowego bloku. Szczegółów szukaj na grafiku.
                                    </td>
                                </tr>
                                <tr>
                                    <td class="discount-cell" style="">
                                        <div class="title">
                                            -25 %
                                        </div>
                                    </td>
                                    <td class="discount-description">
                                        Przy uczęszczaniu równocześnie na kurs Lindy Hop i Solo Jazz obowiązuje 25% zniżki na kurs Solo Jazz!
                                    </td>
                                </tr>
                                <tr>
                                    <td class="discount-cell">
                                        <div class="title">
                                            -50 %
                                        </div>
                                    </td>
                                    <td class="discount-description">
                                        Przy uczęszczaniu równocześnie na kurs więcej niż jednej grupy Lindy Hop (niższe poziomy) obowiązuje 50% zniżki na drugą i kolejną grupę!
                                    </td>
                                </tr>
                            </table>

                            <div class="price-table-explenation">
                                <p>Płatności za zajęcia należy dokonać z góry. Nie ma możliwości odrabiania zjaęć.</p>
                                <p>Nie honorujemy kart typu Multisport, OK System itp.</p>
                            </div>

                        </div>
                    </div>
                </section> <!-- Cennik -->


            </div>

            <div class="bgimage-wrap mtheme-parallax mtheme-modular-column" data-stellar-background-ratio="0.5" style="background-image: url('http://kinetika.imaginem.co/wp-content/uploads/sites/8/2015/02/contactpage_bg2.jpg'); height:auto !important;">

                <div class="container" id="rejestracja">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="section-heading text-align-center">
                                <h1 class="section-title">Rejestracja</h1>
                                <div class="section-end"></div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-10 offset-lg-1 text-align-center mb-5">
                            Przed wypełnieniem formularza rejestracyjnego zachęcamy do zapoznania się z hintami.
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="service-column animated fadeInUp service-column-1 service-boxes-icon-with-title no-border alignicon-top alignicon-top-horizontal serviceboxes-horizontal clearfix animation-action">
                                <div class="service-item animated fadeInUp service-item-space animation-action">
                                    <div data-iconcolor="#fe4641" data-bgcolor="#f9f9f9" class="service-icon">
                                        <i style="color: rgb(254, 70, 65); background-color: rgb(249, 249, 249);" class="fontawesome in-circle  et-icon-circle-compass"></i>
                                    </div>
                                    <div class="service-content">
                                        <h4>Rotacje partnerów</h4>
                                        <div class="service-details">
                                            <p>
                                                Lindy Hop jest tańcem socjalnym i aspekt ten jest niezwykle ważny, dlatego też na zajęciach obowiązuje rotacja partnerów.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="service-column animated fadeInUp service-column-1 service-boxes-icon-with-title no-border alignicon-top alignicon-top-horizontal serviceboxes-horizontal clearfix animation-action">
                                <div class="service-item animated fadeInUp service-item-space animation-action">
                                    <div data-iconcolor="#fe4641" data-bgcolor="#f9f9f9" class="service-icon">
                                        <i style="color: rgb(254, 70, 65); background-color: rgb(249, 249, 249);" class="fontawesome in-circle  et-icon-hotairballoon"></i>
                                    </div>
                                    <div class="service-content">
                                        <h4>Osobne formularze</h4>
                                        <div class="service-details">
                                            <p>
                                                Przy rejestracji w parze konieczne jest wypełnienie DWÓCH ODRĘBNYCH formularzy rejestracyjnych (po jednym dla każdej z osób). Prosimy o dwa oddzielne adresy e-mail – jeden dla partnera, drugi dla partnerki
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="service-column animated fadeInUp service-column-1 service-boxes-icon-with-title no-border alignicon-top alignicon-top-horizontal serviceboxes-horizontal clearfix animation-action">
                                <div class="service-item animated fadeInUp service-item-space animation-action">
                                    <div data-iconcolor="#fe4641" data-bgcolor="#f9f9f9" class="service-icon">
                                        <i style="color: rgb(254, 70, 65); background-color: rgb(249, 249, 249);" class="fontawesome in-circle  et-icon-strategy"></i>
                                    </div>
                                    <div class="service-content">
                                        <h4>Rejestracja w parach</h4>
                                        <div class="service-details">
                                            <p>
                                                Z uwagi na konieczność zachowania proporcji w grupie, zachęcamy do rejestracji w parach. Dotyczy to przede wszystkim partnerek, ponieważ dla nich najczęściej tworzy się lista oczekujących. Dla partnerów bez partnerek z reguły bez problemu znajduje się miejsce w grupie, więc bez przeszkód mogą rejestrować się solo.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="service-column animated fadeInUp service-column-1 service-boxes-icon-with-title no-border alignicon-top alignicon-top-horizontal serviceboxes-horizontal clearfix animation-action">
                                <div class="service-item animated fadeInUp service-item-space animation-action">
                                    <div data-iconcolor="#fe4641" data-bgcolor="#f9f9f9" class="service-icon">
                                        <i style="color: rgb(254, 70, 65); background-color: rgb(249, 249, 249);" class="fontawesome fa fa-money in-circle"></i>
                                    </div>
                                    <div class="service-content">
                                        <h4>Lista oczekujących</h4>
                                        <div class="service-details">
                                            <p>
                                                Rejestracje solo będą umieszczane na liście oczekujących do czasu pojawienia się potwierdzonej płatnością rejestracji osoby o przeciwnej roli w parze (uwaga nie dotyczy zajęć solo).
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6 offset-lg-3">
                            <div class="service-column animated fadeInUp service-column-1 service-boxes-icon-with-title no-border alignicon-top alignicon-top-horizontal serviceboxes-horizontal clearfix animation-action">
                                <div class="service-item animated fadeInUp service-item-space animation-action">
                                    <div data-iconcolor="#fe4641" data-bgcolor="#f9f9f9" class="service-icon">
                                        <i style="color: rgb(254, 70, 65); background-color: rgb(249, 249, 249);" class="fontawesome in-circle  et-icon-strategy"></i>
                                    </div>
                                    <div class="service-content">
                                        <h4>Rejestracja w parach</h4>
                                        <div class="service-details">
                                            <p>
                                                Nie honorujemy kart Multisport, FitProfit itp. Szczegóły na temat płatności znajdują się w formularzu rejestracyjnym.                                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <div class="container clearfix">
                <section id="formularz rejestracyjny">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="section-heading text-align-center">
                                <h1 class="section-title">Formularz rejestracyjny</h1>
                                <div class="section-end"></div>
                            </div>
                        </div>
                    </div>
                    <div class="row mt-5">
                        <div class="col-lg-8 offset-lg-2">
                            <?php
                            echo '<div>';
                            dynamic_sidebar("registration_form");
                            echo '</div>';
                            ?>
                        </div>
                    </div>
                </section> <!-- Rejestracja -->
            </div>
        </div><!-- .entry-content -->
    </div>

    <script>
        var data = {
            "start_hour": "18:00",
            "end_hour": "24:00",
            "days": [
                {
                    "day_name": "Poniedziałek",
                    "classes": [
                        [
                            {
                                "start_time": "19:00",
                                "end_time": "23:30",
                                "name": "Roar Swing!",
                                "place": "Paszcza Lwa",
                                "price": "Kilka złotych monet",
                                "color": "2"
                            },
                        ],
                   ]
                },
                {
                    "day_name": "Wtorek",
                    "classes": [
                        [
                            {
                                "start_time": "20:30",
                                "end_time": "21:45",
                                "name": "LH1A",
                                "place": "Akademia Artystyczna",
                                "color": "3",
                                "instructors": "Ania i Bartek",
                                "group": "Lindy Hop 1A"
                            },
                        ],
                        [
                            {
                                "start_time": "21:00",
                                "end_time": "22:15",
                                "name": "Solo Jazz",
                                "place": "Akademia Artystyczna",
                                "color": "4",
                                "instructors": "Bonk / Myszon",
                                "group": "Solo Jazz"
                            },
                        ],

                    ]
                },
                {
                    "day_name": "Środa",
                    "classes": [
                        [
                            {
                                "start_time": "18:00",
                                "end_time": "19:15",
                                "name": "LH1B",
                                "place": "Gedanus",
                                "color": "3",
                                "instructors": "Marta i Michał",
                                "group": "Lindy Hop 1B"
                            },
                            {
                                "start_time": "19:30",
                                "end_time": "20:45",
                                "name": "LH2",
                                "place": "Gedanus",
                                "color": "3",
                                "instructors":"Marta i Piotrek",
                                "group": "Lindy Hop 2"
                            },
                            {
                                "start_time": "21:00",
                                "end_time": "22:30",
                                "name": "LH2",
                                "place": "Gedanus",
                                "color": "4",
                                "instructors":"Marta i Piotrek",
                                "group": "Lindy Hop 2",
                                "start_date": "01-01-2019",
                                "end_date": "12-01-2019",
                                "registration": "Zarejestruj się`",
                            }
                        ]
                    ]
                },
                {
                    "day_name": "Czwartek",
                    "classes": [
                        [
                            {
                                "start_time": "18:30",
                                "end_time": "19:45",
                                "name": "LH3",
                                "place": "Akademia Artystyczna",
                                "instructors":"Marysia i Tomek",
                                "group": "Lindy Hop 3"
                            },
                            {
                                "start_time": "20:00",
                                "end_time": "21:15",
                                "name": "LH4",
                                "place": "Akademia Artystyczna",
                                "instructors":"Kasia i Tomek",
                                "group": "Lindy Hop 4"
                            },
                            {
                                "start_time": "21:30",
                                "end_time": "22:45",
                                "name": "Bloki tematyczne",
                                "place": "Akademia Artystyczna",
                                "price": "?",
                                "group": "Lindy Hop 2+"
                            }
                        ],
                    ]
                },
                // {
                //     "day_name": "Piątek",
                //     "classes": [
                //         [
                //             {
                //                 "start_time": "18:30",
                //                 "end_time": "19:45",
                //                 "name": "LH3",
                //                 "place": "Akademia Artystyczna",
                //                 "instructors":"Marysia i Tomek",
                //                 "price": "35zł / 120zł",
                //                 "group": "Lindy Hop 3"
                //             },
                //             {
                //                 "start_time": "20:00",
                //                 "end_time": "21:15",
                //                 "name": "LH4",
                //                 "place": "Akademia Artystyczna",
                //                 "instructors":"Kasia i Tomek",
                //                 "price": "35zł / 120zł",
                //                 "group": "Lindy Hop 4"
                //             },
                //             {
                //                 "start_time": "21:30",
                //                 "end_time": "22:45",
                //                 "name": "Bloki tematyczne",
                //                 "place": "Akademia Artystyczna",
                //                 "price": "?",
                //                 "group": "Lindy Hop 2+"
                //             }
                //         ],
                //     ]
                // },

            ]
        };

        var place_map = {
            "Akademia Artystyczna": "/location/akademia-artystyczna",
            "Gedanus": "/location/gedanus",
            "Paszcza Lwa": "/location/paszcza-lwa",
        };

        var group_map = {
            "Lindy Hop 1A": "/group/lh1",
            "Lindy Hop 1B": "/group/lh1",
            "Lindy Hop 2": "/group/lh2",
            "Lindy Hop 3": "/group/lh3",
            "Lindy Hop 4": "/group/lh4",
            "Lindy Hop 2+": "/group/bloki-tematyczne",
            "Solo Jazz": "/group/solo-jazz"

        }

        var data2 = {"start_hour": "19:00", "end_hour": "21:00", "days": [{"classes": [[{"start_time": "20:00", "place": "MIOOTTTTKKK", "end_time": "21:00", "color": "1", "name": "MIOKTTTT"}]], "day_name": "Poniedzia\u0142ek"}, {"classes": [[{"start_time": "19:00", "place": "Siabadaba", "end_time": "20:30", "color": "2", "name": "kottkkk"}]], "day_name": "Wtorek"}]};

        jQuery(document).ready(function($){
            new SchedulePlan($('.cd-schedule'), data, place_map, group_map, 30, 50, 90);
        });
    </script>

<?php get_footer(); ?>