class SchedulePlan {
    constructor(interval, top_line_height, line_height, data) {
        // Interval in minutes
        this.interval = interval;
        this.topLineHeight = top_line_height;
        this.lineHeight = line_height;
        this.data = data

        this.events = jQuery('.events');
        this.timeline = jQuery('.timeline');

    }

    render() {
        this.createDays();
        this.createTimeline();
        this.createClasses();
    }

    createDays() {
        var self = this;
        var days = self.data.days;
        if (jQuery(document).width() > 800){
            var dayWidth = (100 / days.length) - 1 / days.length;  // Summ should be less than 100%
        } else {
            var dayWidth = '100'
        }

        var dayWidthStr = dayWidth.toString() + '%';
        var eventsList = jQuery('<ul>').appendTo(this.events);

        jQuery.each(days, function (key, value) {
            var li = jQuery('<li>').width(dayWidthStr).addClass('events-group').appendTo(eventsList);
            console.log(self.topLineHeight)
            var div = jQuery('<div>').addClass('top-info').height(self.topLineHeight).appendTo(li);
            jQuery('<span>').text(value.day_name).appendTo(div);
        });
    }



    createTimeline() {
        var self = this;
        var startTimelineM = this.timeToMinutes(this.data.start_hour);
        var endTimelineM = this.timeToMinutes(this.data.end_hour);
        var duration = endTimelineM - startTimelineM;

        console.log(duration);

        var timelineList = jQuery('<ul>').appendTo(this.timeline);

        for (var i = 0; i <= duration; i += this.interval) {
            console.log(i, this.interval);
            var li = jQuery('<li>').height(this.interval).appendTo(timelineList);
            jQuery('<span>').text(this.minutesToTime(startTimelineM + i)).appendTo(li);
        }

        if (jQuery(document).width() > 800){
            var setHeight = (duration / self.interval + 1) * self.lineHeight;  // Summ should be less than 100%
        } else {
            var setHeight = 200
        }

        jQuery('.events-group').each(function () {
            jQuery('<ul>').height(setHeight).appendTo(this);
        });

        this.timeline.css("padding-top", this.topLineHeight+4)  // 4 because we have 2px solid borders in title

    }

    createClasses() {
        var self = this;
        var startTimelineM = this.timeToMinutes(self.data.start_hour);
        var eventsgroup = jQuery('.events-group');

        jQuery.each(self.data.days, function (i, day) {
            console.log(day);
            var cols_day = day.classes.length


            var colDayWidth = 100 * (1 / cols_day);
            console.log(cols_day);
            if (cols_day != 0) colDayWidth -= 1;


            var colDayWidthStr = colDayWidth.toString() + '%';

            jQuery.each(day.classes, function (j, classes) {
                jQuery.each(classes, function (k, Class) {
                    console.log(Class);
                    var startEvent = self.timeToMinutes(Class.start_time);
                    var durationEvent = self.timeToMinutes(Class.end_time) - startEvent;
                    var classTop = (startEvent - startTimelineM) * self.lineHeight / self.interval;
                    var classDuration = durationEvent * self.lineHeight / self.interval;
                    var offsetLeft = j * colDayWidth

                    console.log(startEvent, startTimelineM, durationEvent, self.lineHeight, self.interval, classTop)

                    offsetLeft += j * 1

                    var ul = eventsgroup.eq(i).find('ul');

                    var color = (Class.color) ? Class.color : '1';

                    var li = jQuery('<li>').addClass("single-event").attr("data-event", "event-" + color).appendTo(ul);
                    li.css({
                        top: (classTop) + 'px',
                        height: (classDuration + 1) + 'px',
                        width: colDayWidthStr,
                        margin: '0 0 0 ' + offsetLeft + '%'
                    });


                    var ahref = jQuery('<a>').appendTo(li);
                    jQuery('<span>').addClass("event-date").text(Class.start_time + " - " + Class.end_time).appendTo(ahref);
                    jQuery('<span>').addClass("event-place").text(Class.place).appendTo(ahref);
                    jQuery('<em>').addClass("event-name").text(Class.name).appendTo(ahref);

                });
            });
        });
    }

    timeToMinutes(time) {
        var timeArray = time.split(':')
        var timeStamp = parseInt(timeArray[0]) * 60 + parseInt(timeArray[1]);
        return timeStamp;
    }

    minutesToTime(minutes) {
        var o_hours = minutes / 60;
        var o_minutes = (minutes % 60).toString();

        if (o_minutes.length == 1) {
            o_minutes = '0' + o_minutes;
        }

        return o_hours.toString() + ':' + o_minutes.toString()
    }
}