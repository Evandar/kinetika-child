// jQuery(document).ready(function(jQuery){
	var transitionEnd = 'webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend';
	var transitionsSupported = ( jQuery('.csstransitions').length > 0 );
	//if browser does not support transitions - use a different event to trigger them
	if( !transitionsSupported ) transitionEnd = 'noTransition';
	
	//should add a loding while the events are organized 
	var objSchedulesPlan = [];
	var windowResize;

	function SchedulePlan( element, data, place_map, group_map, interval=60, line_height, top_line_height) {
		this.element = element;
		this.data = data;
		this.placeMap = place_map;
		this.groupMap = group_map
		this.interval = interval;
        this.lineHeight = line_height;
        this.topLineHeight = top_line_height;


		this.timeline = this.element.find('.timeline');
        this.timelineStart = getScheduleTimestamp(this.data.start_hour);
        this.timelineEnd = getScheduleTimestamp(this.data.end_hour);
        this.timelineUnitDuration = this.timelineEnd - this.timelineStart;

		this.eventsWrapper = this.element.find('.events');

		this.modal = this.element.find('.event-modal');
		this.modalHeaderBg = this.modal.find('.header-bg');
		this.modalMaxWidth = 500;
		this.modalMaxHeight = 480;

		this.animating = false;

		this.borderSize = 1;
		this.modalExpandWidth = 250;

		objSchedulesPlan.push(this);
		this.initSchedule();
	}

	SchedulePlan.prototype.initSchedule = function() {
        this.initDays();
		this.initTimeline();
		this.initClasses();
        this.scheduleReset();
	};

	SchedulePlan.prototype.scheduleReset = function() {
		var mq = this.mq();
        var eventsGroup = jQuery('.events-group');

        var timelineHeight = (this.timelineUnitDuration / this.interval) * this.lineHeight;
        var dayWidth = (100 / this.data.days.length) + '%'

		if( mq === 'desktop')  {
			//in this case you are on a desktop version (first load or resize from mobile)
            eventsGroup.children('.top-info').outerHeight(this.topLineHeight)
			eventsGroup.children('ul').outerHeight(timelineHeight)
			eventsGroup.outerWidth(dayWidth);

            this.timeline.css("padding-top", this.topLineHeight+2)  // 2 because we have 1px solid borders in title

			this.element.addClass('js-full');
			this.placeEvents();
			this.element.hasClass('modal-is-open') && this.closeModal(this.eventsWrapper.find('.selected-event'));
		}
		else if(  mq === 'mobile' && this.element.hasClass('js-full') ) {
			//in this case you are on a mobile version (first load or resize from desktop)
			this.element.removeClass('js-full loading');
			this.eventsGroup.children('ul').add(this.singleEvents).removeAttr('style');
			this.eventsWrapper.children('.grid-line').remove();
			this.element.hasClass('modal-is-open') && this.checkEventModal();
		} else if( mq === 'desktop' && this.element.hasClass('modal-is-open')){
			//on a mobile version with modal open - need to resize/move modal window
			this.checkEventModal('desktop');
			this.element.removeClass('loading');
		} else {
			this.element.removeClass('loading');
		}
	};

	SchedulePlan.prototype.initDays = function() {
		var self = this;
		var days = self.data.days

        var eventsList = jQuery('<ul>').appendTo(jQuery('.events'));
        jQuery.each(days, function (key, value) {
            var li = jQuery('<li>').addClass('events-group').appendTo(eventsList);
            var div = jQuery('<div>').addClass('top-info').appendTo(li);
            jQuery('<span>').text(value.day_name).appendTo(div);
        });


        self.eventsWrapper.find('.events-group').each(function () {
            jQuery('<ul>').appendTo(this);
        });
    };

	SchedulePlan.prototype.initTimeline = function() {
		var self = this;
        var timelineList = jQuery('<ul>').appendTo(this.timeline);

        for (var i = 0; i<= self.timelineUnitDuration; i += self.interval) {
            var li = jQuery('<li>').height(self.lineHeight).appendTo(timelineList);
            jQuery('<span>').text(getTimeFromTimestamp(self.timelineStart + i)).appendTo(li);
        }

	};

	SchedulePlan.prototype.initClasses = function() {
		var self = this;
		var eventsGroup = self.eventsWrapper.find('.events-group');

        jQuery.each(self.data.days, function (i, day) {
            jQuery.each(day.classes, function (j, classes) {
                jQuery.each(classes, function (k, Class) {
                    var ul = eventsGroup.eq(i).find('ul');
                    var color = (Class.color) ? Class.color : '1';

                    var li = jQuery('<li>')
						.addClass("single-event")
						.attr("data-event", "event-" + color)
                        .attr("data-start", Class.start_time)
                        .attr("data-end", Class.end_time)
                        .attr("data-price", Class.price)
                        .attr("data-place", Class.place)
                        .attr("data-instructors", Class.instructors)
                        .attr("data-group", Class.group)
                        .attr("data-start-date", Class.start_date)
                        .attr("data-end-date", Class.end_date)
                        .attr("data-registration", Class.registration)
                        .attr("data-row", j)
                        .attr("data-row-all", day.classes.length)
						.appendTo(ul);

					if (new Date(Class.start_date) > Date.now())
                    	li.addClass('future');

                    var ahref = jQuery('<a>').appendTo(li);

                    jQuery('<span>').addClass("event-time").text(Class.start_time + " - " + Class.end_time).appendTo(ahref);
                    jQuery('<span>').addClass("event-name").text(Class.name).appendTo(ahref);

                    //detect click on the event and open the modal
                    jQuery(li).on('click', 'a', function(event){
                        event.preventDefault();
                        if( !self.animating ) self.openModal(jQuery(this));
                    });
                });
            });
        });

        //close modal window
        this.modal.on('click', '.close', function(event){
            event.preventDefault();
            if( !self.animating ) self.closeModal(eventsGroup.find('.selected-event'));
        });
        this.element.on('click', '.cover-layer', function(event){
            if( !self.animating && self.element.hasClass('modal-is-open') ) self.closeModal(self.eventsWrapper.find('.selected-event'));
        });
	}

	SchedulePlan.prototype.placeEvents = function() {
		var self = this;
        var singleEvents = this.eventsWrapper.find('.single-event');
        var timelineHeight = jQuery('.events-group ul').outerHeight();
        var dayWidth = jQuery('.events-group').outerWidth();


		singleEvents.each(function(){
			//place each event in the grid -> need to set top position and height
			var start = getScheduleTimestamp(jQuery(this).attr('data-start')),
				duration = getScheduleTimestamp(jQuery(this).attr('data-end')) - start;
            var colWidth = dayWidth * (1 / parseInt(jQuery(this).attr('data-row-all')));
			var eventTop = timelineHeight*(start - self.timelineStart)/self.timelineUnitDuration,
				eventHeight = timelineHeight*duration/self.timelineUnitDuration;

            var offsetLeft = (colWidth)*parseInt(jQuery(this).attr('data-row'))  ;

			jQuery(this).css({
				top: (eventTop -1) +'px',
				height: (eventHeight+1)+'px',
				width: (colWidth - 5)+'px',
                marginLeft: (offsetLeft + 1)+ 'px',

            });
		});

		this.element.removeClass('loading');
	};

	function setOrHide(element, event, param){
		var setValue = event.parent().attr('data-'+param)
		if (setValue) {
            element.find('.event-' + param + ' p').text(setValue)
			element.find('.event-' + param).show()
		} else {
			element.find('.event-' + param).hide()
		}

	}


	function setOrHideDate(element, event) {
		var start_date = event.parent().attr('data-start-date');
		var end_date = event.parent().attr('data-end-date');

		var text = "";


		console.log(end_date)
		if (start_date) {
			text += "od " + start_date + " "
		}

		if (end_date) {
			text += "do " + end_date
		}

		if (text) {
            element.find('.event-date p').text(text)
            element.find('.event-date').show()
		} else {
            element.find('.event-date').hide()
		}

	}

	SchedulePlan.prototype.openModal = function(event) {
		var self = this;
		var mq = self.mq();
		this.animating = true;

		//update event name and time

		this.modal.find('.event-name').text(event.find('.event-name').text());
		this.modal.find('.event-time').text(event.find('.event-time').text());
		this.modal.attr('data-event', event.parent().attr('data-event'));

        setOrHide(this.modal, event, 'place');
        setOrHide(this.modal, event, 'price');
        setOrHide(this.modal, event, 'instructors');
        setOrHide(this.modal, event, 'group');
        setOrHide(this.modal, event, 'start-date');
        setOrHide(this.modal, event, 'registration');

        setOrHideDate(this.modal, event);

        this.modal.find('.details a[href="#"]').on('click', function(evt){
            evt.preventDefault();
        	if( !self.animating ) self.toogleExpandModal(jQuery(this));
		})

        this.modal.find('.details .event-registration').on('click', function(evt){
			self.closeModal(self.eventsWrapper.find('.selected-event'));
            jQuery([document.documentElement, document.body]).animate({
                scrollTop: jQuery("#rejestracja").offset().top
            }, 400);
        })

		this.element.addClass('modal-is-open');

		setTimeout(function(){
			//fixes a flash when an event is selected - desktop version only
			event.parent('li').addClass('selected-event');
		}, 10);

		if( mq == 'mobile' ) {
			self.modal.one(transitionEnd, function(){
				self.modal.off(transitionEnd);
				self.animating = false;
			});
		} else {
			var tableTop = this.element.offset().top - jQuery(window).scrollTop(),
				tableLeft = this.element.offset().left;


			var eventTop = event.offset().top - this.element.offset().top,
                eventLeft = event.offset().left - tableLeft,


				eventHeight = event.innerHeight(),
				eventWidth = event.innerWidth();

			var columnWidth =  jQuery('.events-group').innerWidth(),
                columnLeft = event.closest('.events-group').offset().left - tableLeft;

			var windowWidth = jQuery(window).width(),
				windowHeight = jQuery(window).height();

			var modalWidth = ( windowWidth*.8 > self.modalMaxWidth ) ? self.modalMaxWidth : windowWidth*.8,
				modalHeight = ( windowHeight*.8 > self.modalMaxHeight ) ? self.timeline.height() : windowHeight*.8;


            var modalTranslateX = parseInt(columnLeft - eventLeft + self.borderSize),
                modalTranslateY = -eventTop  + self.topLineHeight;


			//change modal height/width and translate it
			self.modal.css({
				top: eventTop+'px',
				left: eventLeft+'px',
				height: eventHeight+'px',
				width: eventWidth+'px',
				position: 'absolute',
			});
			transformElement(self.modal, 'translateY('+modalTranslateY+'px) translateX('+modalTranslateX+'px)');

            self.modal.on(transitionEnd, function(){
            	//wait for the  end of the modalHeaderBg transformation and show the modal content
                self.modal.width(columnWidth+'px').height(modalHeight+'px')

            });

            self.modal.one(whichTransitionEvent(), function(){
                self.modal.off(transitionEnd);
                self.animating = false;
                self.element.addClass('animation-completed');
                self.modal.width(columnWidth+'px').height(modalHeight+'px')
                self.element.removeClass('content-loaded');
                self.modal.find('.details').show("fast")
            });
		}

        //if browser do not support transitions -> no need to wait for the end of it
		if( !transitionsSupported ) self.modal.add(self.modalHeaderBg).trigger(transitionEnd);
	};

	SchedulePlan.prototype.toogleExpandModal = function(event) {

		if (this.modal.hasClass('expanded')) {
            if (event.attr('class') !== this.modal.attr('expand-source')) {
                this.expandModal(event);
            } else {
				this.compressModal();
			}
		} else {
			this.expandModal(event);
		}

	};

	SchedulePlan.prototype.expandModal = function(event) {
        var self = this;
        this.animating = true;

        var columnWidth = jQuery('.events-group').outerWidth()
        var expandWidth = Math.max(columnWidth, self.modalExpandWidth)
		var map = undefined

        self.modal.find('.details-body').show();

		switch (event.attr('class')) {
			case 'event-place':
				map = self.placeMap;
				break;
			case 'event-group':
				map = self.groupMap;
				break;
		}

        url = map[event.text().replace(/^\s+|\s+$/g, '')]
        this.modal.find('.details-body .content').load(url);
        if (this.modal.offset().left + this.modal.innerWidth() + expandWidth <= self.timeline.offset().left + self.timeline.width()) {
        	var css = {
        		left: columnWidth - 2 * self.borderSize,
                width: expandWidth
            }
		} else {
        	var css = {
                width: expandWidth + 2 * self.borderSize,
                left: -expandWidth - 2 * self.borderSize
			}
		}

        this.modal.find('.details-body').css(css);

        self.modal.one(whichTransitionEvent(), function(){
            self.modal.off(transitionEnd);
            self.animating = false;
            self.modal.addClass('expanded');
            self.modal.attr('expand-source', event.attr('class'))
            self.modal.find('.details-body .content').show()

        });
	};

	SchedulePlan.prototype.compressModal = function() {
		var self = this;
        self.animating = true;

        self.modal.find('.details-body .content').hide()

        self.modal.find('.details-body').css({
			width: self.modal.innerWidth(),
			left: 0,
		});

        self.modal.one(whichTransitionEvent(), function() {
            self.modal.off(transitionEnd);
            self.animating = false;
            self.modal.find('.details-body .content').text("");
            self.modal.removeClass('expanded');
            self.modal.removeAttr('expand-source');
            self.modal.find('.details-body').hide();
        });
	};


	SchedulePlan.prototype.closeModal = function(event) {
		var self = this;
		var mq = self.mq();

		this.animating = true;

		if( mq == 'mobile' ) {
			this.element.removeClass('modal-is-open');
			this.modal.one(transitionEnd, function(){
				self.modal.off(transitionEnd);
				self.animating = false;
				self.element.removeClass('content-loaded');
				event.removeClass('selected-event');
			});
		} else {
			var eventTop = event.offset().top - jQuery(window).scrollTop(),
				eventLeft = event.offset().left,
				eventHeight = event.innerHeight(),
				eventWidth = event.innerWidth();

			var modalTop = Number(self.modal.css('top').replace('px', '')),
				modalLeft = Number(self.modal.css('left').replace('px', ''));

			var modalTranslateX = eventLeft - modalLeft,
				modalTranslateY = eventTop - modalTop;

			self.element.removeClass('animation-completed modal-is-open');
            self.compressModal();
            self.modal.find('.details').hide();
            self.modal.find('.details-body').hide();

			this.modal.css({
				width: eventWidth+'px',
				height: eventHeight+'px'
			});
			transformElement(self.modal, 'translateX('+modalTranslateX+'px) translateY('+modalTranslateY+'px)');
			transformElement(self.modal, 'scaleY(1)');

			this.modal.one(whichTransitionEvent(), function(){
				self.modal.off(transitionEnd);
				self.modal.addClass('no-transition');
				setTimeout(function(){
					self.modal.attr('style', '');
				}, 10);
				setTimeout(function(){
					self.modal.removeClass('no-transition');
				}, 20);

				self.animating = false;
				self.element.removeClass('content-loaded');
				event.removeClass('selected-event');
            });
		}

		//browser do not support transitions -> no need to wait for the end of it
		if( !transitionsSupported ) self.modal.trigger(transitionEnd);
	}

	SchedulePlan.prototype.mq = function(){
		//get MQ value ('desktop' or 'mobile') 
		var self = this;
		return window.getComputedStyle(this.element.get(0), '::before').getPropertyValue('content').replace(/["']/g, '');
	};


	jQuery(window).on('resize', function(){
		if( !windowResize ) {
			windowResize = true;
			(!window.requestAnimationFrame) ? setTimeout(checkResize) : window.requestAnimationFrame(checkResize);
		}
	});

	jQuery(window).keyup(function(event) {
		if (event.keyCode == 27) {
			objSchedulesPlan.forEach(function(element){
				element.closeModal(element.eventsWrapper.find('.selected-event'));
			});
		}
	});

	function whichTransitionEvent(){
		var t,
			el = document.createElement("fakeelement");

		var transitions = {
			"transition"      : "transitionend",
			"OTransition"     : "oTransitionEnd",
			"MozTransition"   : "transitionend",
			"WebkitTransition": "webkitTransitionEnd"
		}

		for (t in transitions){
			if (el.style[t] !== undefined){
				return transitions[t];
			}
		}
	}


	function checkResize(){
		objSchedulesPlan.forEach(function(element){
			element.scheduleReset();
		});
		windowResize = false;
	}

	function getScheduleTimestamp(time) {
		//accepts hh:mm format - convert hh:mm to timestamp
		time = time.replace(/ /g,'');
		var timeArray = time.split(':');
		var timeStamp = parseInt(timeArray[0])*60 + parseInt(timeArray[1]);
		return timeStamp;
	}

	function getTimeFromTimestamp(timestamp) {
		var hours = timestamp / 60;
		var minutes = (timestamp % 60).toString();

		if (minutes.length === 1) {
			minutes = '0' + minutes;
		}

		return hours.toString() + ':' +  minutes;
	}

	function transformElement(element, value) {
		element.css({
		    '-moz-transform': value,
		    '-webkit-transform': value,
			'-ms-transform': value,
			'-o-transform': value,
			'transform': value
		});
	}
// });